# Contributor: Jordan ERNST <SecT0uch@sect0uch.world>
# Maintainer: Jordan ERNST <SecT0uch@sect0uch.world>
pkgname=firewalld
pkgver=1.2.1
pkgrel=1
pkgdesc="A firewall daemon with D-Bus interface providing a dynamic firewall"
url="https://github.com/firewalld/firewalld"
arch="noarch"
license="GPL-2.0-or-later"
# checks produce errors (firewalld needs to be started)
options="!check"
depends="
	dbus
	nftables
	py3-dbus
	py3-gobject3
	py3-nftables
	"
makedepends="
	autoconf
	desktop-file-utils
	docbook-xsl
	gettext
	glib-dev
	intltool
	libxslt
	"
checkdepends="
	iproute2-minimal
	musl-locales
	"
subpackages="
	$pkgname-bash-completion
	$pkgname-doc
	$pkgname-gui
	$pkgname-lang
	$pkgname-openrc
	$pkgname-zsh-completion
	"
source="
	https://github.com/firewalld/firewalld/releases/download/v$pkgver/firewalld-$pkgver.tar.gz
	$pkgname.initd
	"

prepare() {
	default_prepare
}

build() {
	# Disable sytemd and iptables features
	./configure \
		--sysconfdir=/etc \
		--disable-systemd \
		--without-systemd-unitdir \
		--disable-rpmmacros \
		--without-iptables \
		--without-ip6tables \
		--without-ebtables \
		--without-ipset \
		--without-iptables-restore \
		--without-ip6tables-restore \
		--without-ebtables-restore
	make dist
}

package() {
	make DESTDIR="$pkgdir" install

	# Remove useless
	rm -rf "$pkgdir"/etc/sysconfig/
	rm -rf "$pkgdir"/etc/rc.d/

	install -Dm 755 "$srcdir"/$pkgname.initd "$pkgdir"/etc/init.d/$pkgname
}

gui() {
	# Taken here: https://gitweb.gentoo.org/repo/gentoo.git/tree/net-firewall/firewalld/firewalld-1.2.1.ebuild#n181
	pkgdesc="GUI tools and assets for $pkgname"
	amove etc/xdg
	amove usr/bin/firewall-applet
	amove usr/bin/firewall-config
	amove usr/share/applications
	amove usr/share/icons
}


sha512sums="
a9b2d7346291d0c54e850913f2d40a08d121d64df75ee87655fa2ee022ade908c762e5b6d372d4e8879eec1138f73635ee9398cdb3703efe67039769c5e2a42a  firewalld-1.2.1.tar.gz
7a3089c1e34f4b499f463e7adf5ca4f1137994290780ebb08cdcf8728f0933d7236d1aa202c6598c3033d4984d7e113dec102f3b617ad2b766271c60e151d930  firewalld.initd
"
