# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=prettier
pkgver=2.8.0
pkgrel=0
pkgdesc="Opinionated code formatter"
url="https://prettier.io/"
license="MIT"
# armhf, armv7, x86, riscv64: fails to build
arch="noarch !armhf !armv7 !x86 !riscv64"
depends="nodejs"
makedepends="yarn"
checkdepends="npm"
subpackages="$pkgname-doc"
source="https://github.com/prettier/prettier/archive/$pkgver/prettier-$pkgver.tar.gz
	timeout.patch
	"

build() {
	yarn install --frozen-lockfile
	yarn build
}

check() {
	yarn test:dist
}

package() {
	local destdir=/usr/lib/node_modules/prettier

	install -d \
		"$pkgdir"/usr/bin \
		"$pkgdir"/$destdir \
		"$pkgdir"/usr/share/licenses/prettier

	cp -r dist/* "$pkgdir"/$destdir
	ln -s $destdir/bin-prettier.js "$pkgdir"/usr/bin/prettier

	cd "$pkgdir"/$destdir
	rm -r esm # remove ES modules: not needed for command-line usage
	rm README.md # more links to various badges than actual content
	mv LICENSE "$pkgdir"/usr/share/licenses/prettier/LICENSE
}

sha512sums="
5a47e2bbd40ce42ccaa705308335bcc506d85d437d230c16d2ee63624f880e8160ae09a2b5ab4e1630b0651426742fdf2bf8b8c582448d50768ac1a642e49c16  prettier-2.8.0.tar.gz
06968fc076d2cd68360601c84c49b82a1f872a1b54d5ec9738de7ac63793f96cd609817bbb0a0f1902b7ba004232d1276b3577557dc60dd2a7d71fad5e440099  timeout.patch
"
