# Contributor: Lucas Ramage <lucas.ramage@infinite-omicron.com>
# Maintainer: Lucas Ramage <lucas.ramage@infinite-omicron.com>
pkgname=rke
pkgver=1.4.0
pkgrel=0
pkgdesc="Rancher Kubernetes Engine"
options="!check net chmod-clean"
url="https://github.com/rancher/rke"
license="Apache-2.0"
arch="all"
makedepends="go"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/rancher/rke/archive/v$pkgver.tar.gz"

# secfixes:
#   1.3.2-r0:
#     - CVE-2021-25742

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v -ldflags "-X main.VERSION=v$pkgver"
}

package() {
	install -Dm755 $pkgname "$pkgdir"/usr/bin/$pkgname
	install -Dm644 README.md "$pkgdir"/usr/share/doc/$pkgname/README.md
}

sha512sums="
cd45baf11d9af716204dcc96046d6a26fc8e9b46f2f7a6725e6326ad04d6892b24f71402486bd537305ea25cd9b8cfa7fc0fcf02cf05727640924f673bf28836  rke-1.4.0.tar.gz
"
