# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-pep8-naming
pkgver=0.13.2
pkgrel=1
pkgdesc="Check PEP-8 naming conventions plugin for flake8"
url="https://github.com/PyCQA/pep8-naming"
arch="noarch"
license="MIT"
depends="python3 py3-flake8"
makedepends="py3-setuptools"
source="https://files.pythonhosted.org/packages/source/p/pep8-naming/pep8-naming-$pkgver.tar.gz
	flake8-6.patch
	"
builddir="$srcdir/pep8-naming-$pkgver"

replaces="py-pep8-naming" # Backwards compatibility
provides="py-pep8-naming=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	python3 setup.py build
}

check() {
	python3 setup.py test
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
17f407c1c6b70d309e4609da4f60edbb0d59b0842d26c4b17337791289d163d2462e57c135ad8b4ba086bd00c0be4db54c3a1a728ed824efe91b99ea94d616ac  pep8-naming-0.13.2.tar.gz
ab534cc67a250fbb3f2ea04f854a59f15bd823ea4641c677f831e19cab6938213063de105b23425d9ca5685fdffd2f563fecd03c7466dde7ef75d15bd6bab0fa  flake8-6.patch
"
