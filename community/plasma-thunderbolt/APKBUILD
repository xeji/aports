# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-thunderbolt
pkgver=5.26.4
pkgrel=0
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
pkgdesc="Plasma integration for controlling Thunderbolt devices"
license="GPL-2.0-only OR GPL-3.0-only"
depends="bolt"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdeclarative-dev
	ki18n-dev
	knotifications-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-thunderbolt-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # Requires running dbus server

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ad4998a02cb1b08d357bf1b5b9ce4d498382ca76960f9e5406f0a88c2efa22d46e7bc55b0a0bc1821314ed7a03ed2793f6d1c914f2bc93a1a7ba990a820f0131  plasma-thunderbolt-5.26.4.tar.xz
"
