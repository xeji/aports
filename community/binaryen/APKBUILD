# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=binaryen
pkgver=110
pkgrel=0
pkgdesc="Compiler infrastructure and toolchain library for WebAssembly, in C++"
url="https://github.com/WebAssembly/binaryen"
arch="all !s390x"
license="Apache-2.0"
makedepends="
	cmake
	python3
	samurai
	"
checkdepends="
	filecheck
	gtest-dev
	llvm-test-utils
	nodejs
	"
subpackages="$pkgname-dev"
source="https://github.com/WebAssembly/binaryen/archive/version_$pkgver/binaryen-$pkgver.tar.gz
	use-system-gtest.patch
	"
builddir="$srcdir/$pkgname-version_$pkgver"

build() {
	cmake -G Ninja -B build \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_VERBOSE_MAKEFILE=ON \
		-DBUILD_SHARED_LIBS=ON \
		-DBUILD_TESTS=$(want_check && echo ON || echo OFF)
	cmake --build build
}

check() {
	# Tests are extremely chatty.
	msg 'running tests with stdout/stderr redirected into ./check.log...'
	python3 check.py --binaryen-bin build/bin > check.log 2>&1 || {
		echo 'tests failed, printing last 1000 lines of check.log:' >&2
		tail -n 1000 check.log
		return 1
	}
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

dev() {
	default_dev

	# XXX: doesn't exist on riscv64
	amove usr/bin/binaryen-unittests || true
}

sha512sums="
bfe5e8c98409a422145aa64c71d3d5b89bba3d965a3981b9db72a3034daee464606dee117eba3fdcb3e9e34d2fdee8f0a30fa48aaff4d5e34723f20cc01f0197  binaryen-110.tar.gz
ea332616e91c7674c471eb8f8b6352c6d342a17e2b2fd585c6c2b10de89e1be74e63fe42d908af5b2e109d043085e4300905e4efb2ec57bd275db643b22e167d  use-system-gtest.patch
"
