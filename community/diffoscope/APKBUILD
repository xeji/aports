# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=diffoscope
pkgver=227
pkgrel=1
pkgdesc="In-depth comparison of files, archives, and directories"
url="https://diffoscope.org/"
arch="noarch"
license="GPL-3.0-or-later"
depends="
	py3-libarchive-c
	py3-magic
	python3
	"
makedepends="
	py3-docutils
	py3-setuptools
	python3-dev
	"
checkdepends="
	bzip2
	cdrkit
	gzip
	libarchive-tools
	openssh-client-default
	py3-html2text
	py3-pytest
	unzip
	"
source="https://salsa.debian.org/reproducible-builds/diffoscope/-/archive/$pkgver/diffoscope-$pkgver.tar.gz"

build() {
	python3 setup.py build
}

check() {
	# html test fails
	PYTHONPATH="$PWD" \
	PYTHONDONTWRITEBYTECODE=1 \
	pytest -v -k 'not test_diff'
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
73e435b38092b8ffecae5de90d85c379649b60d79b8c5b35937a954e89e14509b73e7db0dafbd5f25d6ff5a259905573375d9b8df9450080d726378e2f910bcb  diffoscope-227.tar.gz
"
