# Contributor: Henrik Riomar <henrik.riomar@gmail.com>
# Contributor: Drew DeVault <sir@cmpwn.com>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=wlroots
pkgver=0.16.0
pkgrel=0
pkgdesc="Modular Wayland compositor library"
url="https://gitlab.freedesktop.org/wlroots/wlroots"
license="MIT"
arch="all"
makedepends="
	eudev-dev
	glslang-dev
	hwdata-dev
	libcap-dev
	libinput-dev
	libseat-dev
	libxcb-dev
	libxkbcommon-dev
	mesa-dev
	meson
	ninja
	pixman-dev
	vulkan-loader-dev
	wayland-dev
	wayland-protocols
	xcb-util-image-dev
	xcb-util-renderutil-dev
	xcb-util-wm-dev
	xkeyboard-config-dev
	xwayland-dev
	"
subpackages="$pkgname-dbg $pkgname-dev"
source="https://gitlab.freedesktop.org/wlroots/wlroots/-/archive/$pkgver/wlroots-$pkgver.tar.gz
	"
options="!check" # no test suite

build() {
	abuild-meson \
		-Db_lto=true \
		-Dexamples=false \
		. build
	meson compile ${JOBS:+-j ${JOBS}} -C build
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C build
}

sha512sums="
0b56f31284cc250019cda1e80d1660031d397e880a8c1aac2e60d7758bcac26e50144a499c13c61e24fe2664ba1e6cbb8262bfe28c817f4664162b1de592f1b6  wlroots-0.16.0.tar.gz
"
