# Contributor: Bhushan Shah <bshah@kde.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwayland-integration
pkgver=5.26.4
pkgrel=0
pkgdesc="KWayland integration"
url="https://kde.org/plasma-desktop/"
arch="all !armhf" # armhf blocked by extra-cmake-modules
license="LGPL-2.1-only OR LGPL-3.0-only"
depends="kglobalaccel"
makedepends="
	extra-cmake-modules
	kguiaddons-dev
	kidletime-dev
	kwayland-dev
	kwindowsystem-dev
	qt5-qtbase-dev
	samurai
	wayland-protocols
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kwayland-integration-$pkgver.tar.xz"
options="!check" # Broken

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
59cf5dcdc8713654221ac5411885be5b656bf1d1cefd344f35b1a3f45e2f036a43633eb474bcc4357783e4e5114afee6c192c63e4c02325d8db9b329b460f1e3  kwayland-integration-5.26.4.tar.xz
"
