# Contributor: Hoang Nguyen <folliekazetani@protonmail.com>
# Contributor: Tuan Hoang <tmhoang@linux.ibm.com>
# Maintainer: Tuan Hoang <tmhoang@linux.ibm.com>
pkgname=yq
pkgver=4.30.4
pkgrel=0
pkgdesc="Portable command-line YAML processor written in Go"
url="https://github.com/mikefarah/yq"
arch="all"
license="MIT"
makedepends="go"
checkdepends="tzdata"
subpackages="
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/mikefarah/yq/archive/v$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v -o yq

	./yq shell-completion bash > yq.bash
	./yq shell-completion zsh > yq.zsh
	./yq shell-completion fish > yq.fish
}

check() {
	go test ./...

	# Yanked from scripts/acceptance.sh
	for test in acceptance_tests/*.sh; do
		echo "--------------------------------------------------------------"
		echo "$test"
		echo "--------------------------------------------------------------"
		sh "$test"
	done
}

package() {
	install -Dm755 yq "$pkgdir"/usr/bin/yq

	install -Dm644 yq.bash \
		"$pkgdir"/usr/share/bash-completion/completions/yq
	install -Dm644 yq.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_yq
	install -Dm644 yq.fish \
		"$pkgdir"/usr/share/fish/completions/yq.fish
}

sha512sums="
88162fe1a914def587045fb12e176045e400130ea12bdd2346669264d5f6591b78cd22b32cbafe3667c4c0660211c5a946d7541cdfaaa896f98fe158fc67f1d6  yq-4.30.4.tar.gz
"
