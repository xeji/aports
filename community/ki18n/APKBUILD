# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ki18n
pkgver=5.100.0
pkgrel=0
pkgdesc="Advanced internationalization framework"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later AND (LGPL-2.1-only OR LGPL-3.0-or-later)"
depends_dev="
	qt5-qtdeclarative-dev
	qt5-qtscript-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	doxygen
	graphviz
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/ki18n-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# ki18n-klocalizedstringtest, kcountrytest, kcountrysubdivisiontest and kcatalogtest are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "(ki18n-klocalizedstring|kcountry|kcountrysubdivision|kcatalog)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
18d8c9a8e80fa8a1229a6cfa6c8de02044a406ad5af0491461c6e66ad15182a9700f43478e7ec9e90e36cc3d4f20b8e9650ba29aadfa64ce67d61de859dfb5e1  ki18n-5.100.0.tar.xz
"
