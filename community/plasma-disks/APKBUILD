# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-disks
pkgver=5.26.4
pkgrel=0
pkgdesc="Monitors S.M.A.R.T. capable devices for imminent failure"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends="
	kirigami2
	smartmontools
	"
makedepends="
	extra-cmake-modules
	qt5-qtbase-dev
	kcoreaddons-dev
	kdbusaddons-dev
	knotifications-dev
	ki18n-dev
	solid-dev
	kservice-dev
	kio-dev
	kauth-dev
	kdeclarative-dev
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-disks-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
cf6eaaa23b89a742ca4101f5b8d8d2cf4df215a1c4559730730db75a5c8c87cac20fe75d4d188c981cceff6d5532376a4aa973b5d805a024c612ab44096cfdb6  plasma-disks-5.26.4.tar.xz
"
