# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-remotecontrollers
pkgver=5.26.4
pkgrel=0
pkgdesc="Translate various input device events into keyboard and pointer events"
url="https://invent.kde.org/plasma-bigscreen/plasma-remotecontrollers"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="BSD-2-Clause AND (GPL-2.0-only OR GPL-3.0-only) AND CC0-1.0"
# For some reason there is no dep on so:libcec but it crashes without it
depends="libcec"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kconfig-dev
	kdeclarative-dev
	ki18n-dev
	knotifications-dev
	kpackage-dev
	kwindowsystem-dev
	libcec-dev
	libevdev-dev
	plasma-workspace-dev
	qt5-qtbase-dev
	qt5-qtwayland-dev
	samurai
	solid-dev
	"
case "$CARCH" in
	ppc64le) ;;
	*) makedepends="$makedepends xwiimote-dev" ;;
esac

subpackages="$pkgname-lang"
case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-remotecontrollers-$pkgver.tar.xz
	uinput.conf
	"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	install -Dm644 \
		"$srcdir"/uinput.conf \
		-t "$pkgdir"/etc/modules-load.d
}

sha512sums="
2960ce927dbb3d518151a67918265bd9dbbe290075335011a43b9c756084585916e879c1a11b56dde8dab962898c035afcd08c25c72118548ae1ce741ad41ffc  plasma-remotecontrollers-5.26.4.tar.xz
a9b069ed121ffeee887e0583d8cb46035ecf1fa90a26a4ecb3aa11ff03178b2b08621f6676db6b2350f290694c04aabcf36f2ce3e0813a76dde9a33555edb112  uinput.conf
"
