# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kjobwidgets
pkgver=5.100.0
pkgrel=0
pkgdesc="Widgets for tracking KJob instances"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends_dev="
	kcoreaddons-dev
	kwidgetsaddons-dev
	qt5-qtx11extras-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kjobwidgets-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0f1e96ee2a38f366ac583d7bb7426d409d11d428ba473ae3706e6c8baddcc8e1f8b3f90da05083622f2c23cc3d02f0c707e26e9edfafc6f1a2d655a4eb13369a  kjobwidgets-5.100.0.tar.xz
"
