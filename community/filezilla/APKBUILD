# Maintainer: Leonardo Arena <rnalrd@alpinelinux.org>
pkgname=filezilla
pkgver=3.62.1
pkgrel=0
pkgdesc="FTP Client"
url="https://filezilla-project.org/"
arch="all"
license="GPL-2.0-or-later"
makedepends="
	autoconf
	automake
	gnutls-dev
	gtk+3.0-dev
	libfilezilla-dev>=0.30.0
	libidn-dev
	libtool
	pugixml-dev
	sqlite-dev
	wxwidgets-dev
	xdg-utils
	"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.filezilla-project.org/client/FileZilla_${pkgver}_src.tar.bz2
	wxwidgets-3.2.patch
	"
case "$CARCH" in
	s390x) options="$options !check";; # ratelimit_test fails
	riscv64) options="$options textrels";; # temp allow textrels
esac

prepare() {
	default_prepare

	# patched autoconf
	autoreconf -vif
}

build() {
	case "$CARCH" in
	x86)
		# fails to build otherwise
		export CFLAGS="$CFLAGS -D_FORCE_SOFTWARE_SHA"
		;;
	esac
	./configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--without-dbus \
		--disable-manualupdatecheck \
		--disable-autoupdatecheck
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
ac69596a528b2e4140dd9b24c00aba7c08c06c9e6ad50c33ffef5b88686e5487420d02dfb79dd00e8b8afd545125096abfe6d49d10561e64fd120e95670a13f1  FileZilla_3.62.1_src.tar.bz2
751328020399e3ab6e9de48ebeaac3c8d20b74c264e4b13a1d9afa3fca75407ea3c446707d9289fb4b5e05516ec6ca2492be4c523432b1645cd99b4f649835ab  wxwidgets-3.2.patch
"
