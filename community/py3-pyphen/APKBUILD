# Contributor: Stefan Wagner <stw@bit-strickerei.de>
# Maintainer: Drew DeVault <sir@cmpwn.com>
pkgname=py3-pyphen
_pkgname=Pyphen
pkgver=0.13.1
pkgrel=1
pkgdesc="python-based dictionary hyphenator library"
url="https://www.pyphen.org"
arch="noarch"
license="GPL-2.0-or-later AND LGPL-2.1-or-later AND MPL-1.1"
depends="python3"
makedepends="py3-gpep517 py3-flit-core"
checkdepends="py3-pytest py3-pytest-cov py3-pytest-isort py3-pytest-xdist"
source="$_pkgname-$pkgver.tar.gz::https://github.com/Kozea/Pyphen/archive/$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

replaces="py-pyphen" # Backwards compatibility
provides="py-pyphen=$pkgver-r$pkgrel" # Backwards compatibility

prepare() {
	default_prepare
	# disable pytest-flake8
	# https://github.com/tholo/pytest-flake8/issues/87
	sed -i 's/--flake8//' pyproject.toml
}

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/pyphen-$pkgver-py3-none-any.whl
}

sha512sums="
230aec57f0a2db11f30b15115e5ed59ce19aed60120de24783f58ef831854ad7e236ae1da086bca01f9a41cef24f8402d9e0bf9251f6d55c38aa624fd55d4a3b  Pyphen-0.13.1.tar.gz
"
